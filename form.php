<?php
$genders = array(0 => "Nam", 1 => "Nữ");
$colleges = array('TCT' => 'Toán Cơ Tin Học', 'DL' => 'Địa lý', 'S' => 'Sinh', 'MT' => 'Môi trường');
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="index.css">
	<link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
    <script src= "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" ></script>
    <title>Day 05: Đăng ký tân sinh viên</title>
</head>
<body>
    <form method="POST" action="form.php" enctype="multipart/form-data">
		<div class="box-error-message">
			<?php
				$currentDirectory = getcwd();
    			$uploadDirectory = "/uploads";
				if (!is_dir($currentDirectory.$uploadDirectory)) {
					mkdir($currentDirectory.$uploadDirectory, 0777, true);
				}
				$uploadOk = 1;
				$validate = true;
				$fileName = $_FILES['file']['name'];
				$uploadPath = $currentDirectory . $uploadDirectory .'/'. basename($fileName);
				$imageFileType = strtolower(pathinfo($uploadPath,PATHINFO_EXTENSION));
				if ($_SERVER["REQUEST_METHOD"] == "POST") {
					if (!isset($_POST["username"]) || empty($_POST["username"])) {
						echo "<p style=\"color: red\">Hãy nhập tên</p>";
						$validate = false;
					}
					if (!isset($_POST["gender"])) {
						echo "<p style=\"color: red\">Hãy chọn giới tính</p>";
						$validate = false;
					}
					if (!isset($_POST["college"]) || empty($_POST["college"])) {
						echo "<p style=\"color: red\">Hãy chọn phân khoa</p>";
						$validate = false;
					}
					if (!isset($_POST["dob"]) || empty($_POST["dob"])) {
						echo "<p style=\"color: red\">Hãy nhập ngay sinh</p>";
						$validate = false;
					}else {
						if (preg_match("/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/", $_POST["dob"]) === 0) {
							echo "<p style=\"color: red\">Hãy nhập ngay sinh đúng định dạng</p>";
							$validate = false;
						}
					}
					if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
						echo "Chỉ được tải ảnh đuôi JPG, JPEG, PNG & GIF lên.";
						$uploadOk = 0;
						$validate = false;
					}
					if ($uploadOk != 0) {
						$fileTmpName  = $_FILES['file']['tmp_name'];
						move_uploaded_file($fileTmpName,$uploadPath) or die( "Không thể copy file vào thư mục!");
					}
					if ($validate) {
						session_start();
						$_SESSION = $_POST;
						$_SESSION["file"] = $_FILES['file']['name'];
						header('location: resSubmit.php');
					}
				}
			?>
		</div>
        <div class="m-row">
            <div class="label">
                <label for="name" class="required">Họ và tên</label>
            </div>
            <input class="text-input" type="text" id="username" name="username">
        </div>
        <div class="m-row">
            <div class="label">
                <label for="gender" class="required">Giới tính</label>
            </div>
			<div class="gender-div">
				<?php
					for ($i = 0; $i < count($genders); ++$i) {
						echo ("
							<div>
								<input type=\"radio\" name=\"gender\" value=\"$i\">
								<label>$genders[$i]</label>
							</div>
						");
					}
				?>
			</div>
        </div>
		<div class="m-row">
            <div class="label">
                <label for="college" class="required">Phân khoa</label>
            </div>
			<select name="college" id="college">
				<?php
					echo "<option></option>";
					foreach ($colleges as $college ) {
						echo "<option value=\"$college\">$college</option>";
					}
				?>
			</select>
        </div>
		<div class="m-row">
			<div class="label">
				<label for="dob" class="required">Ngày sinh</label>
			</div>
			<input class="date-picker" type="text" id="dob" name="dob" placeholder="dd/mm/yyyy">
		</div>
		<div class="m-row">
			<div class="label">
				<label for="address">Địa chỉ</label>
			</div>
			<input class="text-input" type="text" id="address" name="address">
		</div>
		<div class="box-img-show">
			<div class="label">
				<label for="img">Hình ảnh</label>
			</div>
			<div class="box-img">
				<input type="file" id="file" name="file" accept="image/*" value=''>
			</div>
		</div>
        <div class="submit-div">
            <input type="submit" value="Đăng ký"></input>
        </div>
    </form>
	
	<script>
		$(document).ready(function() {
			$(function() {
				$("#dob").datepicker({
					dateFormat: 'dd/mm/yy'
				});
			});
		})
	</script>
</body>
</html>
